package edu.nwmissouri.leet;

import android.animation.ValueAnimator;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by S521805 on 11/10/2015.
 */
public class StudentsListAdapter extends ArrayAdapter<StudentListView.Student> {
    private static final int DEFAULT_THRESHOLD = 128;
    public StudentsListAdapter(Context context, int resource, int textViewResourceId, List ideas) {
        super(context, resource, textViewResourceId, ideas);
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        View view = super.getView(position, convertView, parent);
        // Getting resources of text views from list item
        TextView ideaTextTV = (TextView)view.findViewById(R.id.text);
        // TextView secName  = (TextView) view.findViewById(R.id.rating);
        // Setting texts of text views of list view
        ideaTextTV.setText(getItem(position).getFirstName()+" "+getItem(position).getLastName());
        // secName.setText(getItem(position).getSecName());

        return view;
    }
}

