package edu.nwmissouri.leet;

import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewConfiguration;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView studentsLV;
    Spinner spinner;
    String secName;
    StudentsListAdapter studentsListAdapter;
    private SharedPreferences sharedPreferences;
    ArrayList<StudentListView.Student> studentsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Notification notification;

        Calendar calender = Calendar.getInstance();
        int month = calender.get(Calendar.DAY_OF_MONTH);
        if (month == 1) {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
            builder.setSmallIcon(R.drawable.face);
            builder.setContentTitle("Leet Center:");
            builder.setContentText("Please Export The Data And Reset Each Student Data.");
            builder.setStyle(new NotificationCompat.BigTextStyle().bigText("Please Export The Data And Reset Each Student Data.")); // Same as above
            builder.setAutoCancel(true);
            builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);

            notification = builder.build();

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notification.defaults = Notification.DEFAULT_ALL;
            notificationManager.cancel(10);
            notification.flags |= Notification.FLAG_AUTO_CANCEL | Notification.FLAG_ONLY_ALERT_ONCE;
            notificationManager.notify(10, notification);
        }

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        // fab.setBackgroundColor();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Starting AddAnStudent activity
                Intent intent = new Intent(MainActivity.this, AddAnStudent.class);
                startActivity(intent);
            }
        });
        spinner = (Spinner) findViewById(R.id.spinner1);
        secName = spinner.getSelectedItem().toString();
        studentsLV = (ListView) findViewById(android.R.id.list);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                secName = spinner.getSelectedItem().toString();
                Iterator<StudentListView.Student> list = StudentListView.studentsList.iterator();
                studentsList = new ArrayList<>();

                while (list.hasNext()) {
                    StudentListView.Student idea = list.next();
                    if (idea.getSecName().equals(spinner.getSelectedItem().toString())) {
                        studentsList.add(idea);
                    }
                }
                studentsListAdapter = new StudentsListAdapter(MainActivity.this, R.layout.idea_list_item, R.id.text, studentsList);
                studentsLV.setAdapter(studentsListAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        studentsLV.setOnItemClickListener(new CustomOnItemClickListener());
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Starting async task to retrieve all ideas stored in parse
        RetrieveStudents retrieveStudents = new RetrieveStudents();
        retrieveStudents.setStudentTaskCallback(studentTaskCallback);
        retrieveStudents.execute();
    }


    RetrieveStudents.StudentTaskCallback studentTaskCallback = new RetrieveStudents.StudentTaskCallback() {
        @Override
        public void updateList() {
            // Updating list view
            Iterator<StudentListView.Student> list = StudentListView.studentsList.iterator();
            studentsList.clear();

            while (list.hasNext()) {
                StudentListView.Student student = list.next();
                if (student.getSecName().equals(secName)){
                    studentsList.add(student);
                }
            }
            studentsListAdapter = new StudentsListAdapter(MainActivity.this, R.layout.idea_list_item, R.id.text, studentsList);
            studentsLV.setAdapter(studentsListAdapter);
            studentsLV.setOnItemClickListener(new CustomOnItemClickListener());
        }
    };

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            ParseUser.logOut();
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putBoolean(AConstants.IS_Logged_IN, false);
            edit.apply();
            finish();
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            return true;
        }
        if(id == R.id.action_reset) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("Are you sure you want to delete the data?");
            builder.setNegativeButton("Delete All", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ParseUser currentUser = ParseUser.getCurrentUser();
                    String username = currentUser.getUsername();
                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Students");
                    query.whereEqualTo("username", username);
                    query.findInBackground(new FindCallback<ParseObject>() {
                        public void done(List<ParseObject> ideasList, ParseException e) {
                            if (e == null) {
                                for (ParseObject idea : ideasList) {
                                    idea.deleteInBackground();
                                }
                                StudentListView.studentsList.clear();
                                studentsList.clear();
                                studentsListAdapter = new StudentsListAdapter(MainActivity.this, R.layout.idea_list_item, R.id.text, studentsList);
                                studentsLV.setAdapter(studentsListAdapter);
                            } else {

                            }
                        }
                    });
                }
            });
            builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.show();
        }
        if(id == R.id.action_export){
            String FilePath = Environment.getExternalStorageDirectory() + File.separator
                    + "Stats.csv";
            Intent sendIntent = new Intent(android.content.Intent.ACTION_SEND);
            sendIntent.setType("application/csv");
            sendIntent.putExtra(android.content.Intent.EXTRA_EMAIL, ParseUser.getCurrentUser().getEmail());
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, "");
            sendIntent.putExtra(Intent.EXTRA_TEXT, "");
            File f = new File(FilePath);
            try {
                BufferedWriter bufferedWriter;
                if(f.exists() && !f.isDirectory()){
                    f.delete();
                    f = new File(FilePath);
                    FileWriter fw = new FileWriter(f.getAbsoluteFile());
                    bufferedWriter = new BufferedWriter(fw);
                }else{
                    FileWriter fw = new FileWriter(f.getAbsoluteFile());
                    bufferedWriter = new BufferedWriter(fw);
                }
                for(StudentListView.Student student:StudentListView.studentsList){
                    bufferedWriter.write(student.getSecName()+","+student.getFirstName()+","+student.getLastName()+","+student.getLunch()+","+student.getMeal()+","+student.getGuestMeal());
                    bufferedWriter.newLine();
                }
                bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Uri U = Uri.fromFile(f);
            sendIntent.putExtra(Intent.EXTRA_STREAM, U);
            startActivity(Intent.createChooser(sendIntent, "Send Mail"));
        }

        return super.onOptionsItemSelected(item);
    }

    public class CustomOnItemClickListener implements OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {

            Intent i = new Intent(MainActivity.this, DetailActivity.class);
            i.putExtra("STUDENT_FIRST_NAME",studentsList.get(position).getFirstName());
            i.putExtra("STUDENT_LAST_NAME",studentsList.get(position).getLastName());
            i.putExtra("STUDENT_LUNCH",studentsList.get(position).getLunch());
            i.putExtra("STUDENT_GUEST_MEAL",studentsList.get(position).getGuestMeal());
            i.putExtra("STUDENT_MEAL",studentsList.get(position).getMeal());
            i.putExtra("secName",secName);
            i.putExtra("iD", studentsList.get(position).getID());
            startActivity(i);


        }
    }

}
