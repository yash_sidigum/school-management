package edu.nwmissouri.leet;

import android.os.AsyncTask;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

/**
 * Created by S521805 on 11/10/2015.
 */
public class RetrieveStudents extends AsyncTask<Void, Void, Void> {

    StudentTaskCallback studentTaskCallback;

    public void setStudentTaskCallback(StudentTaskCallback studentTaskCallback) {
        this.studentTaskCallback = studentTaskCallback;
    }

    @Override
    protected Void doInBackground(Void... params) {

        // Removing all items from studentsList
        StudentListView.studentsList.clear();
        ParseUser currentUser = ParseUser.getCurrentUser();
        String username = currentUser.getUsername();
        // Parse query to retrieve all ideas stored in parse
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Students");
        query.whereEqualTo("username",username);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> ideasList, ParseException e) {
                if (e == null) {
                    for (ParseObject idea : ideasList) {
                        String firstName = idea.getString("firstName");
                        String lastName = idea.getString("lastName");
                        String secName = idea.getString("secName");
                        int meal = idea.getInt("meal");
                        int lunch = idea.getInt("lunch");
                        int guestMeal = idea.getInt("guestMeal");
                        int iD = idea.getInt("iD");
                        StudentListView.Student ideaObject = new StudentListView.Student(firstName, lastName, secName, lunch, guestMeal, meal, iD);
                        StudentListView.studentsList.add(ideaObject);
                        // Updating list view
                        studentTaskCallback.updateList();
                    }

                } else {
                    Log.d("Retrieve Students","Error: " + e.getMessage().toString());
                }
            }
        });

        return null;
    }


    public interface StudentTaskCallback {
        // A Callback method to communicate with main activity
        void updateList();
    }

}
