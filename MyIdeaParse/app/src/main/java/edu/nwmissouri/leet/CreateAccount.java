package edu.nwmissouri.leet;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class CreateAccount extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

    }

    public void signUpBTN(View view){
        EditText usernameET = (EditText)findViewById(R.id.userNameEditText);
        final EditText passwordET = (EditText)findViewById(R.id.passwordEditText);
        final EditText confirmPasswordET = (EditText)findViewById(R.id.confirmPasswordEditText);
        EditText emailET = (EditText)findViewById(R.id.emailEditText);

        String username = usernameET.getText().toString();
        String password = passwordET.getText().toString();
        String confirmPassword = confirmPasswordET.getText().toString();
        String email = emailET.getText().toString();

        if(username.trim().equals("") || password.equals("")|| confirmPassword.equals("") || email.equals("")){
            AlertDialog.Builder builder = new AlertDialog.Builder(CreateAccount.this);
            builder.setMessage("Please enter all the fields");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.show();
        }
        else if(!password.equals(confirmPassword)){
            AlertDialog.Builder builder = new AlertDialog.Builder(CreateAccount.this);
            builder.setMessage("Password mismatch");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    confirmPasswordET.setText("");
                }
            });
            builder.show();
        }
        else{
            ParseUser user = new ParseUser();
            user.setUsername(username);
            user.setPassword(password);
            user.setEmail(email);

            // other fields can be set just like with ParseObject
            user.put("phone", "650-253-0000");

            user.signUpInBackground(new SignUpCallback() {
                public void done(ParseException e) {
                    if (e == null) {
                        // Hooray! Let them use the app now.
                        ParseUser.logOutInBackground();
                        finish();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(CreateAccount.this);
                        builder.setMessage(e.getMessage().toString());
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        builder.show();
                    }
                }
            });

        }
    }

    public void cancelBTN(View view){
        finish();
    }
}
