package edu.nwmissouri.leet;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import com.parse.ParseObject;
import com.parse.ParseUser;

import mehdi.sakout.fancybuttons.FancyButton;


/**
 * Created by S521805 on 11/10/2015.
 */
public class AddAnStudent extends AppCompatActivity {



    private EditText firstName;
    private EditText lastName;
    private Spinner spinner;
    private Button doneBTN;
    private Button cancelBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_idea);

        // Getting Resources from activity_add_idea
        firstName = (EditText) findViewById(R.id.studentFirstName);
        lastName = (EditText) findViewById(R.id.studentLastName);
        spinner = (Spinner) findViewById(R.id.spinner1);
        doneBTN = (Button) findViewById(R.id.doneBTN);
        cancelBTN = (Button) findViewById(R.id.cancelBTN);
        doneBTN.setEnabled(false);

        lastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if ((firstName.getText().toString().length() > 0) && (lastName.getText().toString().length() > 0)) {
                    doneBTN.setEnabled(true);

                }


                if ((firstName.getText().toString().length() == 0) && (lastName.getText().toString().length() == 0)) {
                    doneBTN.setEnabled(false);
                }
            }
        });
        firstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if ((firstName.getText().toString().length() > 0) && (lastName.getText().toString().length() > 0)) {
                    doneBTN.setEnabled(true);

                }


                if ((firstName.getText().toString().length() == 0) && (lastName.getText().toString().length() == 0)) {
                    doneBTN.setEnabled(false);

                }
            }
        });
        doneBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = 0;
                for (StudentListView.Student student : StudentListView.studentsList) {
                    if (student.getSecName().equals(spinner.getSelectedItem().toString())) {
                        count++;
                    }
                }
                ParseUser currentUser = ParseUser.getCurrentUser();
                String username = currentUser.getUsername().toString();
                ParseObject ideaObject = new ParseObject("Students");
                ideaObject.put("secName", spinner.getSelectedItem().toString());
                ideaObject.put("firstName", lastName.getText().toString());
                ideaObject.put("lastName", firstName.getText().toString());
                ideaObject.put("lunch", 0);
                ideaObject.put("meal", 0);
                ideaObject.put("guestMeal", 0);
                ideaObject.put("username", username);
                ideaObject.put("iD", count + 1);
                ideaObject.saveInBackground();
                // Dismissing this activity
                finish();
            }
        });

        cancelBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Dismissing this activity
                finish();
            }
        });

    }



}
