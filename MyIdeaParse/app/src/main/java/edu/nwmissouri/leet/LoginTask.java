package edu.nwmissouri.leet;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

/**
 * Created by S521805 on 11/28/2015.
 */
public class LoginTask extends AsyncTask<String, Void, Void> {

    LoginTaskCallback loginTaskCallback;

    public void setLoginTaskCallback(LoginTaskCallback loginTaskCallback) {
        this.loginTaskCallback = loginTaskCallback;
    }

    @Override
    protected Void doInBackground(String... params) {

        ParseUser.logInInBackground(params[0], params[1], new LogInCallback() {
            public void done(ParseUser user, ParseException e) {
                if (user != null) {
                    if(!user.getBoolean("emailVerified")){

                        loginTaskCallback.emailVerificationAlert();
                    }
                    else{
                        loginTaskCallback.loginSuccessFull();
                    }
                } else {
                    loginTaskCallback.loginFailed();
                }
            }
        });
        return null;
    }

    public interface LoginTaskCallback {
        void loginSuccessFull();
        void loginFailed();
        void emailVerificationAlert();
    }
}
