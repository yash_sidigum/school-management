package edu.nwmissouri.leet;

import java.util.ArrayList;

/**
 * Created by S521805 on 11/10/2015.
 */
public class StudentListView {

    public static ArrayList<Student> studentsList = new ArrayList<>();

    public static class Student{
        private String firstName;
        private String lastName;
        private String secName;
        private int lunch;
        private int meal;
        private int guestMeal;
        private int iD;

        public Student(String firstName, String lastName, String secName, int lunch, int guestMeal, int meal, int iD) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.secName = secName;
            this.lunch = lunch;
            this.guestMeal = guestMeal;
            this.meal = meal;
            this.iD = iD;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getSecName() {
            return secName;
        }

        public int getLunch() {
            return lunch;
        }

        public int getMeal() {
            return meal;
        }

        public int getGuestMeal() {
            return guestMeal;
        }

        public int getID(){ return iD;}

        public void setLunch(int lunch) {
            this.lunch = lunch;
        }

        public void setMeal(int meal) {
            this.meal = meal;
        }

        public void setGuestMeal(int guestMeal) {
            this.guestMeal = guestMeal;
        }
    }
}
