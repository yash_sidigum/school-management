package edu.nwmissouri.leet;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

public class DetailActivity extends AppCompatActivity {

    private StudentListView.Student currentStudent;

    EditText firstNameET;
    EditText lastNameET;
    private TextView text2;
    private CheckBox lunchCB;
    private CheckBox mealCB;
    private CheckBox guestMealCB;

    private int lunchCount;
    private int mealCount;
    private int guestMealCount;

    private Button doneBTN;
    private Button cancelBTN;
    private Button resetBTN;
    private Button editBTN;
    private Button saveBTN;
    private String secName;
    private int iD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        final String firstName = getIntent().getStringExtra("STUDENT_FIRST_NAME");
        String lastName = getIntent().getStringExtra("STUDENT_LAST_NAME");
        secName = getIntent().getStringExtra("secName");
        int lunch = getIntent().getIntExtra("STUDENT_LUNCH", 0);
        final int guest_meal = getIntent().getIntExtra("STUDENT_GUEST_MEAL", 0);
        int meal = getIntent().getIntExtra("STUDENT_MEAL", 0);
        iD = getIntent().getIntExtra("iD",0);

        for (StudentListView.Student student : StudentListView.studentsList) {
            if (student.getFirstName().equals(firstName) && student.getLastName().equals(lastName) && student.getSecName().equals(secName) && student.getID() == iD) {
                this.currentStudent = student;
            }
        }

        doneBTN = (Button) findViewById(R.id.Button02);
        cancelBTN = (Button) findViewById(R.id.Button03);
        resetBTN = (Button) findViewById(R.id.Button04);
        editBTN = (Button) findViewById(R.id.Button05);
        saveBTN = (Button) findViewById(R.id.Button06);
        lunchCount = currentStudent.getLunch();
        mealCount = currentStudent.getMeal();
        guestMealCount = currentStudent.getGuestMeal();

        firstNameET = (EditText) findViewById(R.id.firstNameET);
        firstNameET.setText(firstName);
        lastNameET = (EditText) findViewById(R.id.lastNameET);
        lastNameET.setText(lastName);
        firstNameET.setEnabled(false);
        lastNameET.setEnabled(false);
        text2 = (TextView) findViewById(R.id.textView3);
        text2.setText("Milk: " + meal + "   " + "Lunch: " + lunch + "   " + "Guest Meal: " + guest_meal);

        lunchCB = (CheckBox) findViewById(R.id.lunch);
        mealCB = (CheckBox) findViewById(R.id.meal);
        guestMealCB = (CheckBox) findViewById(R.id.guest_meal);

        lunchCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    lunchCount++;
                } else {
                    lunchCount--;
                    if(lunchCount < 0){
                        lunchCount = 0;
                    }
                }
                text2.setText("Milk: " + mealCount + "   " + "Lunch: " + lunchCount + "   " + "Guest Meal: " + guestMealCount);
            }
        });

        mealCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mealCount++;
                } else {
                    mealCount--;
                    if(mealCount < 0){
                        mealCount = 0;
                    }
                }
                text2.setText("Milk: " + mealCount + "   " + "Lunch: " + lunchCount + "   " + "Guest Meal: " + guestMealCount);
            }
        });

        guestMealCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    guestMealCount++;
                } else {
                    guestMealCount--;
                    if(guestMealCount < 0){
                        guestMealCount = 0;
                    }
                }
                text2.setText("Milk: " + mealCount + "   " + "Lunch: " + lunchCount + "   " + "Guest Meal: " + guestMealCount);
            }
        });

        doneBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseUser currentUser = ParseUser.getCurrentUser();
                String username = currentUser.getUsername();
                // Parse query to retrieve all ideas stored in parse
                ParseQuery<ParseObject> query = ParseQuery.getQuery("Students");
                query.whereEqualTo("username", username);
                query.whereEqualTo("secName", secName);
                query.findInBackground(new FindCallback<ParseObject>() {
                                           public void done(List<ParseObject> ideasList, ParseException e) {
                                               if (e == null) {
                                                   for (ParseObject idea : ideasList) {
                                                       if (idea.getInt("iD") == iD) {
                                                           idea.put("lunch", lunchCount);
                                                           idea.put("meal", mealCount);
                                                           idea.put("guestMeal", guestMealCount);
                                                           idea.saveInBackground();
                                                           for (StudentListView.Student student : StudentListView.studentsList) {
                                                               if (student.getSecName().equals(secName) && student.getID() == iD) {
                                                                   student.setLunch(lunchCount);
                                                                   student.setGuestMeal(guestMealCount);
                                                                   student.setMeal(mealCount);

                                                               }
                                                           }
                                                           finish();
                                                       }
                                                   }
                                               } else {
                                                   Log.d("Retrieve Students", "Error: " + e.getMessage().toString());
                                               }
                                           }
                                       }

                );

            }
        });

        editBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstNameET.setEnabled(true);
                firstNameET.setSelection(firstNameET.getText().toString().length());

                lastNameET.setEnabled(true);
                editBTN.setVisibility(View.GONE);
                saveBTN.setVisibility(View.VISIBLE);
            }
        });

        saveBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                editBTN.setVisibility(View.VISIBLE);
                saveBTN.setVisibility(View.GONE);
                final String firstname = firstNameET.getText().toString().trim();
                final String lastname = lastNameET.getText().toString().trim();
                firstNameET.setEnabled(false);
                lastNameET.setEnabled(false);
                if(!firstname.equals("") && !lastname.equals("")){
                    ParseUser currentUser = ParseUser.getCurrentUser();
                    String username = currentUser.getUsername();
                    // Parse query to retrieve all ideas stored in parse
                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Students");
                    query.whereEqualTo("username", username);
                    query.whereEqualTo("secName", secName);
                    query.findInBackground(new FindCallback<ParseObject>() {
                                               public void done(List<ParseObject> ideasList, ParseException e) {
                                                   if (e == null) {
                                                       for (ParseObject idea : ideasList) {
                                                           if (idea.getInt("iD") == iD) {
                                                               idea.put("firstName", firstname);
                                                               idea.put("lastName", lastname);
                                                               idea.saveInBackground();
                                                               for (StudentListView.Student student : StudentListView.studentsList) {
                                                                   if (student.getSecName().equals(secName) && student.getID() == iD) {
                                                                       student.setFirstName(firstname);
                                                                       student.setLastName(lastname);
                                                                   }
                                                               }
                                                           }
                                                       }
                                                   } else {
                                                       Log.d("Retrieve Students", "Error: " + e.getMessage().toString());
                                                   }
                                               }
                                           }
                    );
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailActivity.this);
                    builder.setMessage("Please enter first name and last name.");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();
                }
            }
        });


        cancelBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        resetBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseUser currentUser = ParseUser.getCurrentUser();
                String username = currentUser.getUsername();
                // Parse query to retrieve all ideas stored in parse
                ParseQuery<ParseObject> query = ParseQuery.getQuery("Students");
                query.whereEqualTo("username", username);
                query.whereEqualTo("secName", secName);
                query.findInBackground(new FindCallback<ParseObject>() {
                                           public void done(List<ParseObject> ideasList, ParseException e) {
                                               if (e == null) {
                                                   for (ParseObject idea : ideasList) {
                                                       if (idea.getInt("iD") == iD) {
                                                           idea.put("lunch", 0);
                                                           idea.put("meal", 0);
                                                           idea.put("guestMeal", 0);
                                                           idea.saveInBackground();
                                                           for (StudentListView.Student student : StudentListView.studentsList) {
                                                               if (student.getSecName().equals(secName) && student.getID() == iD) {
                                                                   student.setLunch(0);
                                                                   student.setGuestMeal(0);
                                                                   student.setMeal(0);
                                                                   text2.setText("Milk: " + 0 + "   " + "Lunch: " + 0 + "   " + "Guest Meal: " + 0);
                                                                   lunchCount = 0;
                                                                   mealCount = 0;
                                                                   guestMealCount = 0;
                                                               }
                                                           }
                                                       }
                                                   }
                                               } else {
                                                   Log.d("Retrieve Students", "Error: " + e.getMessage().toString());
                                               }
                                           }
                                       }
                );
            }
        });

    }

}