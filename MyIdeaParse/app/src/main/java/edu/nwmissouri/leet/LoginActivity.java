package edu.nwmissouri.leet;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.parse.ParseUser;

import mehdi.sakout.fancybuttons.FancyButton;

public class  LoginActivity extends AppCompatActivity {

    private EditText userNameET;
    private EditText passwordET;
    private Button loginBTN;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);

        if (sharedPreferences.getBoolean(AConstants.IS_Logged_IN, false)) {
            finish();
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        } else {

            userNameET = (EditText) findViewById(R.id.userNameET);
            passwordET = (EditText) findViewById(R.id.passwordET);
            loginBTN = (Button) findViewById(R.id.loginBTN);
            loginBTN.setEnabled(false);

            userNameET.addTextChangedListener(new TextWatcher() {
                @Override
                public void afterTextChanged(Editable arg0) {
                    if ((userNameET.getText().toString().length() > 0) && (passwordET.getText().toString().length() > 0)) {
                        loginBTN.setEnabled(true);
                    }

                    if ((userNameET.getText().toString().length() == 0) && (passwordET.getText().toString().length() == 0)) {
                        loginBTN.setEnabled(false);
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
            });

            passwordET.addTextChangedListener(new TextWatcher() {
                @Override
                public void afterTextChanged(Editable arg0) {
                    if ((userNameET.getText().toString().length() > 0) && (passwordET.getText().toString().length() > 0)) {
                        loginBTN.setEnabled(true);

                    }

                    if ((userNameET.getText().toString().length() == 0) && (passwordET.getText().toString().length() == 0)) {
                        loginBTN.setEnabled(false);

                    }

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
            });


            loginBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LoginTask loginTask = new LoginTask();
                    loginTask.setLoginTaskCallback(loginTaskCallback);
                    loginTask.execute(userNameET.getText().toString(), passwordET.getText().toString());
                }
            });
        }
    }

    public void createAccountButton(View view){
        Intent intent = new Intent(this,CreateAccount.class);
        startActivity(intent);
    }

    public void forgotPasswordBTN(View view){
        Intent intent = new Intent(this,ForgotPassword.class);
        startActivity(intent);
    }

    LoginTask.LoginTaskCallback loginTaskCallback = new LoginTask.LoginTaskCallback() {
        @Override
        public void loginSuccessFull() {
            finish();
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putBoolean(AConstants.IS_Logged_IN, true);
            edit.apply();
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        }

        @Override
        public void emailVerificationAlert() {
            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
            builder.setMessage("Please verify your mail to Login");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ParseUser.logOut();
                }
            });
            builder.show();
        }

        @Override
        public void loginFailed() {
            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
            builder.setTitle("Login Failed");
            builder.setMessage("Invalid login credentials.");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.show();
        }
    };
}
